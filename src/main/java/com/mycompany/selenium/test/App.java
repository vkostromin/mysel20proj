package com.mycompany.selenium.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.ExpectedCondition;
//import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class App {

	public static void main(String[] args) throws InterruptedException {
		// Create a new instance of the Firefox driver
		// Notice that the remainder of the code relies on the interface,
		// not the implementation.
		WebDriver driver = new FirefoxDriver();

		for (int year = 2012; year <= 2025; ++year) {
			driver.get("http://www.marketwatch.com/optionscenter/calendar/" + year);
			// Find the text input element by its name
			List<WebElement> months = driver.findElements(By.className("calendar"));

			if (months.size() != 12) {
				System.out.println("Error");
			}

			for (int i = 0; i < 12; ++i) {
				List<WebElement> elements = months.get(i).findElements(By.className("quarterly-expiration"));// expiration
				for (int j = 0; j < elements.size(); ++j) {
					System.out.println("('" + year + "-"
							+ String.format("%02d", (i + 1)) + "-"
							+ String.format("%02d", Integer.parseInt(elements.get(j).getText()))
							+ "'),"
					);
				}
			}

			//Close the browser
		}
		driver.quit();
	}
}
